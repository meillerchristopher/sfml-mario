/****************************************************************
* Ce jeu est codé avec la SFML 2.4.2 avec le compilateur Mingw
* Il est fait pour fonctionner avec du 1280 x 736
*****************************************************************/

#include "Game.h"

int main()
{
    //Instanciation de game
    Game* game = new Game();

    delete game;

    return 0;
}
