#ifndef GAME_H
#define GAME_H

/**********************************************************
* Classe Principale du jeu
***********************************************************/

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <vector>
#include "../include/Tilmap.h"
#include "../include/Personnage.h"
#include "../include/Utils.h"
#include <iostream>
#include <windows.h>

class Game
{
    public:
        Game();
        ~Game();
        void Update();

    private:
        sf::Clock chrono, clock;
        sf::Font font;
        sf::Text times, fps, sound;
        sf::Music musique, Sound_title;
        sf::RenderWindow game_window;
        sf::Event event;
        sf::Texture Texture_Title, Texture_Mouse;
        sf::Sprite Sprite_Title, Sprite_Mouse;
        std::vector<Player*>enemies, game_player;
        std::vector<char>level, x, y;
        TileMap map;
        char width[6] = "WIDTH", volume[7] = "VOLUME", height[7] = "HEIGHT", title[6] = "TITLE", fps_limite[11] = "FPS_LIMITE", vsync[6] = "Vsync";

        void Draw();
        void Move_Ennemies();
        void Reset();
        void Modify_Sound();
        void Scene_Title();
        void Show_Title(std::string picture);
        void Main_Menu();
        void Compile();
        void Scene_Setting();

};
#endif // GAME_H
