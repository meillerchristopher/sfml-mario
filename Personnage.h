#ifndef PERSONNAGE_H
#define PERSONNAGE_H

/****************************************************************************************************************
* Classe de gestion des personnages
****************************************************************************************************************/

#include <SFML/Graphics.hpp>
#include <vector>
#include<iostream>

class Player{
    public:
        Player(std::string imgDirectory);

        ~Player();

        void setPosition(unsigned short x, unsigned short y);

        void drawPlayer(sf::RenderWindow &window);

        void movePlayer(std::vector<char>&level);

        void random_move(std::vector<char>&level);

        void AnimationMove(void);

    private:
        sf::Texture pTexture;
        sf::Sprite pSprite;
        sf::Clock chrono;

        const int SPEED = 8, GRAVITYSPEED = 32, JUMPSPEED = 32;
        bool teste = false;
        enum Dir {Down, Left, Right, Up};
        sf::Vector2i anim;

        bool colid(std::vector<char>level, unsigned short x, unsigned short y, unsigned short offset);
        bool Gravity(std::vector<char>level, unsigned short x, unsigned short y);
        bool Jump(std::vector<char>level, unsigned short x, unsigned short y);

};

#endif // PERSONNAGE_H
