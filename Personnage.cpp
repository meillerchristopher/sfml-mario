#include "../include/Personnage.h"

Player::Player(std::string imgDirectory)
    {
        pTexture.loadFromFile(imgDirectory);
        pTexture.setSmooth(true);
        pSprite.setTexture(pTexture);
        pSprite.setPosition(0, 0);
    }

Player::~Player()
    {
        delete this;
    }

void Player::drawPlayer(sf::RenderWindow &window)
    {
        window.draw(pSprite);
    }

void Player::setPosition(unsigned short x, unsigned short y)
    {
        pSprite.setPosition(x, y);
    }

void Player::AnimationMove(void)
    {
        if((unsigned int)anim.x * 32 >= pTexture.getSize().x)anim.x = 0;
        pSprite.setTextureRect(sf::IntRect(anim.x*32, anim.y*32, 32, 32));
    }

bool Player::colid(std::vector<char>level, unsigned short x, unsigned short y, unsigned short offset = 1)
    {

    }

bool Player::Gravity(std::vector<char>level, unsigned short x, unsigned short y)
    {
        return(level.at(int(y*1.25 + x/32 + 40)) == 4 ? true : false);
    }

bool Player::Jump(std::vector<char>level, unsigned short x, unsigned short y)
    {
        return((level.at(int(y*1.25+x/32)-40) == 4 && level.at(int(y*1.25+x/32)+40) < 4 ? true : false));
    }

void Player::random_move(std::vector<char>&level)
    {
        /*unsigned short x = pSprite.getPosition().x, y = pSprite.getPosition().y;
        if(teste)
        {
            if(colid(level, x, y, 0))
            {
                teste = false;
            }else{
                x--;
                anim.x++;
                anim.y = Left;
            }
        }else{
            if(colid(level, x, y))
            {
                teste = true;
            }else{
                x++;
                anim.x++;
                anim.y = Right;
            }
        }
        pSprite.setPosition(x, y);*/
    }

void Player::movePlayer(std::vector<char>&level)
    {
    sf::Time time = chrono.getElapsedTime();

    unsigned short xPlayer = pSprite.getPosition().x, yPlayer = pSprite.getPosition().y;
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && pSprite.getPosition().x > 0 && time.asMilliseconds() < 500)
        {
            anim.y = Left;
            anim.x++;
            colid(level, xPlayer, yPlayer, 0) ? xPlayer = xPlayer : xPlayer -= SPEED;
            chrono.restart();
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            anim.y = Right;
            anim.x++;
            colid(level, xPlayer, yPlayer) ? xPlayer = xPlayer : xPlayer+=SPEED;
            chrono.restart();
        }
        pSprite.setPosition(xPlayer, yPlayer);
    }
