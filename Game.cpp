#include "Game.h"

/*********************************************************************************************************************
 * Constructeur de la class Game
 ********************************************************************************************************************/

Game::Game()
{
    //Cr�ation des personnages
    game_player.push_back(new Player("ressources/Characters/player.png"));
    game_player.at(0)->setPosition(0, 672);

    // on d�finit le niveau � l'aide d'un fichier texte
    Load_Level("level1.bin", level, enemies , map);

    //Chargement de font puis assignement des position et des couleurs et du font
    font.loadFromFile("ressources/Fonts/font.ttf");

    //Attribution du volume de base et mise en route de la musique
    musique.setVolume(Load_Options_Int(volume));
    musique.play();

    Texture_Mouse.loadFromFile("ressources/Pictures/cursor.png");
    Texture_Mouse.setSmooth(true);
    Sprite_Mouse.setTexture(Texture_Mouse);

    // on cr�e la fen�tre
    game_window.create(sf::VideoMode(Load_Options_Int(width), Load_Options_Int(height)+16), Load_Options_Str(title));
    game_window.setFramerateLimit(Load_Options_Int(fps_limite));
    game_window.setVerticalSyncEnabled(Load_Options_Int(vsync));

    std::cout << "Time to boot game : " << chrono.getElapsedTime().asSeconds() << "s" << std::endl;
    Scene_Title();
}

/***********************************************************************************************************************
 * Destructeur de la class Game
 ***********************************************************************************************************************/

Game::~Game()
{
   for(unsigned char i = enemies.size()-1; i > 0; i--)
       delete enemies.at(i);

    delete enemies.at(0);
    delete game_player.at(0);
}

/************************************************************************************************************************
 * Mise � jour du d�placement des personnages
 ***********************************************************************************************************************/

void Game::Move_Ennemies()
{
    for(unsigned int i = 0; i < enemies.size(); i++)
    {
        enemies.at(i)->random_move(level);
        enemies.at(i)->AnimationMove();
    }
    game_player.at(0)->AnimationMove();
}

/************************************************************************************************************************
 * Dessin du jeu
 ***********************************************************************************************************************/

void Game::Draw()
{
        //Mis a jour de lecran et dessin de tout
        game_window.clear();
        game_window.draw(map);
        //Volume du jeu
        Draw_Text("Volume: " + nb2string((short)musique.getVolume()),font,Load_Options_Int(width)-400,0,game_window,1,1);
        //times
        Draw_Text(nb2string((short)chrono.getElapsedTime().asSeconds()), font, Load_Options_Int(width)-40, 0, game_window, 1, 1);
        //FPS
        Draw_Text(nb2string(short(1/clock.restart().asSeconds())), font, 0, 0, game_window, 1, 1);
        game_player.at(0)->drawPlayer(game_window);

        for(unsigned short i = 0; i < enemies.size(); i++)
             enemies.at(i)->drawPlayer(game_window);

        game_window.display();
}

/***********************************************************************************************************************
 * Mise � jour du jeu
 ***********************************************************************************************************************/

void Game::Update()
{
    while(game_window.isOpen())
    {
          while(game_window.pollEvent(event))
          {
              //fermeture du jeu
              if(event.type == sf::Event::Closed)
                game_window.close();

              //reset du jeu
              if(sf::Keyboard::isKeyPressed(sf::Keyboard::F12))
                Reset();

              //Modifi le son du jeu
              Modify_Sound();

              game_player.at(0)->movePlayer(level);
          }
          Move_Ennemies();
          Draw();
    }
    this->~Game();
}

/************************************************************************************************************************
 * Reset le jeu
 ************************************************************************************************************************/

void Game::Reset()
{
    std::cout << "------Reset-------" << std::endl;
    game_window.close();
    //Construction
    Game* game = new Game();
    //Destruction
    this->~Game();
    //Mise � jour
    game->Update();
}

/*************************************************************************************************************************
 * Modifie le son de la musique en plein jeu
 *************************************************************************************************************************/

 void Game::Modify_Sound()
 {
     if(sf::Keyboard::isKeyPressed(sf::Keyboard::Add) && musique.getVolume() < 100)
        musique.setVolume(int(musique.getVolume())+2);

     if(sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract))
        musique.setVolume(int(musique.getVolume())-1);
 }

 /*************************************************************************************************************************
  * Scene d'�cran titre
  *************************************************************************************************************************/

 void Game::Scene_Title()
 {
     //Affichage des splash
     Show_Title("ressources/title/titre.png");
     Show_Title("ressources/title/titre2.png");

     //Reduction de la taille de l'image
     Sprite_Title.setScale(0,0);

     //Appel du menu principal
     Main_Menu();
 }

 void Game::Show_Title(std::string picture)
 {
     //Chargement
     Texture_Title.loadFromFile(picture);

     //Lissage
     Texture_Title.setSmooth(true);

     //Assignation � un Sprite
     Sprite_Title.setTexture(Texture_Title);

     //Dessin
     game_window.draw(Sprite_Title);
     game_window.display();

     //Temps de transition en millis
     Sleep(2000);
 }

 /*************************************************************************************************************************
  * Menu Principal
  *************************************************************************************************************************/

 void Game::Main_Menu()
 {
  int sel = 0;
  sf::Texture Texture_Menu;
  sf::Sprite Sprite_Menu;
  Texture_Menu.loadFromFile("ressources/Pictures/fond.png");
  Texture_Menu.setSmooth(true);
  Sprite_Menu.setTexture(Texture_Menu);

  //gestion des d�placements dans le menu
  sf::Time time = chrono.getElapsedTime();

  while(game_window.isOpen())
    {
         while(game_window.pollEvent(event))
          {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && time.asMilliseconds()>300)
                {
                    sel < 4 ? sel++ : sel = 0;
                    chrono.restart();
                }
                else
                    if(sf::Mouse::getPosition().y >=300 && sf::Mouse::getPosition().y < 800 && event.type == sf::Event::MouseMoved)
                        sel = sf::Mouse::getPosition().y/100-3;

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && time.asMilliseconds()>300)
                {
                    sel > 0 ? sel-- : sel = 4;
                    chrono.restart();
                }
                else
                    if(sf::Mouse::getPosition().y >=300 && sf::Mouse::getPosition().y < 800 && event.type == sf::Event::MouseMoved)
                        sel = sf::Mouse::getPosition().y/100-3;

            if(event.type == sf::Event::Closed)
                this->~Game();

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return) || sf::Mouse::isButtonPressed(sf::Mouse::Left))

                switch(sel)
                {
                //nouveau jeu
                case 0:
                    this->Update();
                    break;

                //continue
                case 1:
                    std::cout << "continue" << std::endl;
                    break;

                //option
                case 2:
                    Scene_Setting();
                    break;

                //quitter
                case 3:
                    this->~Game();
                    break;

                //compiler
                case 4:
                    Compile();
                    break;
                }
            }
            //Gestion des dessins
            game_window.setMouseCursorVisible(false);
            Sprite_Mouse.setPosition(static_cast<sf::Vector2f>(sf::Mouse::getPosition(game_window)));

            game_window.draw(Sprite_Menu);

            Make_Choice(200, 80, "New Game", 500, 100, game_window, font, 100+sel*100);
            Make_Choice(200, 80, "Continue", 500, 200, game_window, font, 100+sel*100);
            Make_Choice(200, 80, "Setting",  500, 300, game_window, font, 100+sel*100);
            Make_Choice(200, 80, "Exit",     500, 400, game_window, font, 100+sel*100);
            Make_Choice(200, 80, "Compile",  500, 500, game_window, font, 100+sel*100);

            game_window.draw(Sprite_Mouse);
            game_window.display();
    }
    delete this;
 }

/*************************************************************************************************************************
* Methode de compilation ingame du jeu
*************************************************************************************************************************/

 void Game::Compile()
 {
    system("Compile.bat");
 }

 /*************************************************************************************************************************
  * Menu Option
  *************************************************************************************************************************/

 void Game::Scene_Setting()
 {

 }

