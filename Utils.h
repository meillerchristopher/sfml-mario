#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <SFML/Audio.hpp>

#include <iostream>
#include <vector>
#include <fstream>
#include <string.h>
#include "../include/Personnage.h"
#include "../include/Tilmap.h"

struct structure
{
    int tab[920], n[920];
    char name[10], enemie_name[20], level_name[20];
};

void ReadBin(const std::string &File, structure &s);

void WriteBin(structure s, const std::string &File);

bool file_exists(const std::string &File);

void Load_Level(const std::string &File, std::vector<char>&level, std::vector<Player*>&enemies, TileMap &mape);

unsigned short Load_Options_Int(char* Option);
std::string Load_Options_Str(char* Option);
std::string nb2string(unsigned short nb);
void Make_Choice(int width, int height, std::string text, int pos_x, int pos_y, sf::RenderWindow &window, sf::Font font, double blabla);
void Draw_Text(std::string text, sf::Font font, int pos_x, int pos_y, sf::RenderWindow &window, int height, int width);

#endif // UTILS_H_INCLUDED
