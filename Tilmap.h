#ifndef TILMAP_H_INCLUDED
#define TILMAP_H_INCLUDED

/***************************************************************************************************************************************************
* Classe gérant le dessin du niveau
****************************************************************************************************************************************************/

#include <SFML/Graphics.hpp>
#include <iostream>

class TileMap : public sf::Drawable, public sf::Transformable
{
    public:
        bool load(const std::string& tileset, sf::Vector2u tileSize, std::vector<char> *tiles, unsigned short width, unsigned short height);
        void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    private:
        sf::VertexArray m_vertices;
        sf::Texture m_tileset;
};

#endif // TILMAP_H_INCLUDED
