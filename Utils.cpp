#include <windows.h>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include "Utils.h"

/*************************************************************************************************************************************
 * Fonctions de chargement des options stocker dans config.ini
**************************************************************************************************************************************/

unsigned short Load_Options_Int(char* Option)
{
    return GetPrivateProfileIntA(TEXT(Option), TEXT(Option), 0, TEXT(".\\Config.ini"));
}

std::string Load_Options_Str(char* Option)
{
    char takenstring[100];
    GetPrivateProfileString(TEXT(Option), TEXT(Option), TEXT("fail while retrieving"), takenstring, 10, TEXT(".\\Config.ini"));
    return takenstring;
}

/*************************************************************************************************************************************
* Fonction de conversion de nombre en string
**************************************************************************************************************************************/

std::string nb2string(unsigned short nb)
{
    std::ostringstream s;
    s << nb;
    return s.str();
}

/*************************************************************************************************************************************
* Fonction de chargement du niveau
**************************************************************************************************************************************/

void ReadBin(const std::string &File, structure &s)
{
    std::ifstream fic(File, std::ios::binary);

    fic.read((char*)&s, sizeof (structure));

    fic.close();
}

void WriteBin(structure s, const std::string &File)
{
    std::ofstream fic(File, std::ios::binary);

    fic.write((const char *)&s,sizeof (structure));

    fic.close();
}

bool file_exists(const std::string &File)
{
    std::ifstream fic(File, std::ios::binary);

    return fic.good();
}

void Load_Level(const std::string &File, std::vector<char>&level, std::vector<Player*>&enemies, TileMap &mape)
{
    structure s2;


    std::string level_name = "levels/" + File;

    if(file_exists(level_name))
    {
        ReadBin(level_name, s2);

        for(int i = 0; i < 920; i++)
            level.push_back(s2.tab[i]);

        for(int i = 0; i < 920; i++)
            level.push_back(s2.n[i]);
    }
    else
    {
        std::cout << "le fichier " << File << " na pas ete trouve\nGeneration du fichier " << File << " en cour..." << std::endl;

        //Génération des tiles et des ennemies
        for(int i = 0; i < 920; i++)
        {
            s2.tab[i] = 4;
            s2.n[i] = 0;
        }
        s2.n[919] = 1;
        s2.n[919-39] = 1;

        //attribution du nom du level
        strcpy(s2.name, File.c_str());

        strcpy(s2.enemie_name, "enemie");

        strcpy(s2.level_name, "tilset");

        //écriture du level
        WriteBin(s2, level_name);

        //lecture du level
        ReadBin(level_name, s2);

        for(int i = 0; i < 920; i++)
            level.push_back(s2.tab[i]);

        for(int i = 0; i < 920; i++)
            level.push_back(s2.n[i]);

        std::cout << "Generation termine" << std::endl;
    }

    /******************************************************************************
     Chargement des et positionnement des ennemies
    ******************************************************************************/

    int cmpt = 0;
    std::string chara = "ressources/Characters/", tilset = "ressources/Tilsets/";

    chara.append(s2.enemie_name);
    chara.append(".png");

    tilset.append(s2.level_name);
    tilset.append(".png");

    for(int i = 920; i < 1840; i++)
    {
        if((int)level.at(i) == 1)
            {
                enemies.push_back(new Player(chara));
                enemies.at(cmpt)->setPosition(((i  - 920) % 40) * 32, int((i - 920) / 40) * 32);
                cmpt++;
            }
    }

    // on crée la tilemap avec le niveau précédemment défini
    mape.load(tilset, sf::Vector2u(32, 32), &level, 40, 23);

}

/*************************************************************************************************************************************
* Fonction ajoutant un choix au menu principal
**************************************************************************************************************************************/

void Make_Choice(int width, int height, std::string text, int pos_x, int pos_y, sf::RenderWindow &window, sf::Font font, double blabla)
 {
    sf::Text text_in_rect;

    text_in_rect.setFont(font);
    text_in_rect.setPosition(pos_x, pos_y);
    text_in_rect.setString(text);
    text_in_rect.setFillColor(sf::Color::Black);

    text_in_rect.scale((blabla == pos_y ? 3 : 2), 2);

    window.draw(text_in_rect);
 }

/*************************************************************************************************************************************
* Fonction permettant de dessiner du text facilement
**************************************************************************************************************************************/

void Draw_Text(std::string text, sf::Font font, int pos_x, int pos_y, sf::RenderWindow &window, int height, int width)
{
    sf::Text texte;
    texte.setFont(font);
    texte.setString(text);
    texte.setScale(width, height);
    texte.setPosition(pos_x , pos_y);
    window.draw(texte);
}
